class Fixnum

  def in_words
    conversion(self)
  end



  private
  def conversion (number)
    if number < 10
      conversion_under_ten(number)
    elsif number < 13
      conversion_ten_to_twelve(number)
    elsif number < 20
      conversion_teens(number)
    elsif number < 100 && number%10 == 0
      conversion_tens(number)
    elsif number < 100
      conversion_less_than_hundred(number)
    elsif number < 1000
      conversion_less_than_thousand(number)
    elsif number < 1000000
      conversion_less_than_million(number)
    elsif number < 1000000000
      conversion_less_than_billion(number)
    elsif number < 1000000000000
      conversion_less_than_trillion(number)
    else
      conversion_more_than_trillion(number)
    end
  end

  def conversion_under_ten(number)
    digit = {0 => 'zero', 1 => 'one', 2 => 'two', 3 =>'three', 4 => 'four',
    5 => 'five', 6 => 'six', 7 => 'seven', 8 => 'eight', 9 => 'nine'}

    digit[number]
  end

  def conversion_ten_to_twelve(number)
    digit = {10 => 'ten', 11 => 'eleven', 12 => 'twelve'}

    digit[number]
  end

  def conversion_teens (number)
    digit = {13 => 'thirteen', 14 =>'fourteen', 15 => 'fifteen', 16 => 'sixteen',
    17=>'seventeen', 18=>'eighteen', 19=>'nineteen'}

    digit[number]
  end

  def conversion_tens (number)
    digit = {20 => 'twenty', 30 => 'thirty', 40 => 'forty', 50 => 'fifty',
    60 => 'sixty', 70 => 'seventy', 80 => 'eighty', 90 =>'ninety'}

    digit[number]
  end

  def conversion_less_than_hundred (number)
    tenth = (number/10) * 10
    single = number%10

    "#{conversion(tenth)} #{conversion(single)}"
  end

  def conversion_less_than_thousand (number)
    hundred = (number/100)
    tenth = number%100
    if tenth != 0
      "#{conversion(hundred)} hundred #{conversion(tenth)}"
    else
      "#{conversion(hundred)} hundred"
    end
  end

  def conversion_less_than_million (number)
    thousand = number/1000
    hundreds = number%1000

    if hundreds != 0
      "#{conversion(thousand)} thousand #{conversion(hundreds)}"
    else
      "#{conversion(thousand)} thousand"
    end
  end

  def conversion_less_than_billion (number)
    millions = number/1000000
    thousands = number%1000000

    if thousands != 0
      "#{conversion(millions)} million #{conversion(thousands)}"
    else
      "#{conversion(millions)} million"
    end
  end

  def conversion_less_than_trillion (number)
    billions = number/1000000000
    millions = number%1000000000

    if millions != 0
      "#{conversion(billions)} billion #{conversion(millions)}"
    else
      "#{conversion(billions)} billion"
    end
  end

  def conversion_more_than_trillion(number)
    trillions = number/1000000000000
    billions = number%1000000000000

    if billions != 0
      "#{conversion(trillions)} trillion #{conversion(billions)}"
    else
      "#{conversion(trillions)} trillion"
    end
  end

end
